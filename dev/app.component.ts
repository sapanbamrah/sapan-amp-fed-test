import {Component} from 'angular2/core';
import {SearchPeopleComponent} from './search/component/search-people.component';

@Component({
    selector: 'my-app',
    templateUrl: 'templates/app.tpl.html',
    directives:[SearchPeopleComponent]
})
export class AppComponent {

}

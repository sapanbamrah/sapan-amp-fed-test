import {Component} from 'angular2/core';
import {SearchItemComponent} from './search-item.component';
import {PeopleListService} from '../service/people-list.service';
import {SearchPipe} from '../pipe/search-pipe';
 
@Component({
    selector: '[search-result]',
    templateUrl: 'templates/search/search-result.tpl.html',
    directives:[SearchItemComponent],
    providers:[PeopleListService],
    pipes:[SearchPipe]
})
export class SearchResultComponent implements OnInit{
    itemsCount:number;
    searchItems: Array<SearchItem>;
    response:string;
    searchText = '';
    response = Array<SearchItem>;
  
    constructor(private _peopleListService: PeopleListService){}
    
    ngOnInit():any{
        this.searchItems = this._peopleListService.fetchList();
        this.itemsCount = this.searchItems.length;
    }
  
}

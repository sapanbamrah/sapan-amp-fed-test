import {Component, Input} from 'angular2/core';

@Component({
    selector: 'search-item',
    templateUrl: 'templates/search/search-item.tpl.html'
})
export class SearchItemComponent {
    @Input() searchItem;
}

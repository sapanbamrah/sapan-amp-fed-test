import {Component} from 'angular2/core';
import {SearchResultComponent} from './search-result.component';
 
@Component({
    selector: '[search-people]',
    templateUrl: 'templates/search/search-people.tpl.html',
    directives:[SearchResultComponent]
})
export class SearchPeopleComponent {

}

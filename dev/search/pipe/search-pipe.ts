import {Pipe} from 'angular2/core';
import {PipeTransform} from 'angular2/core';
import {SearchItem} from '../interface/search-item';

@Pipe({
    name:'searchPipe'
})
export class SearchPipe implements PipeTransform{
    transform(value:SearchItem[], args:string[]):any{
        if(value.length === 0){
            return value;
        }
        
        let resultArray = [];
        
        for(let item of value){
            if(item.firstName.match('^.*'+ args[0] + '.*$')){
                resultArray.push(item); 
            }
        }
        
        return resultArray;
    }
}

export interface SearchItem{
    id:number,
    firstName: string,
    lastName:string,
    picture:string,
    Title:string
}
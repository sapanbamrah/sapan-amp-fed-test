System.register(['angular2/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var SearchBoxComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            SearchBoxComponent = (function () {
                function SearchBoxComponent() {
                    this.searchChanged = new core_1.EventEmitter();
                }
                SearchBoxComponent.prototype.searchEvent = function (searchText) {
                    this.searchChanged.emit(searchText);
                };
                SearchBoxComponent = __decorate([
                    core_1.Component({
                        selector: '[search-box]',
                        templateUrl: 'templates/search/search-box.tpl.html',
                        outputs: ['searchChanged']
                    }), 
                    __metadata('design:paramtypes', [])
                ], SearchBoxComponent);
                return SearchBoxComponent;
            }());
            exports_1("SearchBoxComponent", SearchBoxComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlYXJjaC9jb21wb25lbnQvc2VhcmNoLWJveC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFPQTtnQkFBQTtvQkFFSSxrQkFBYSxHQUFHLElBQUksbUJBQVksRUFBVSxDQUFDO2dCQUsvQyxDQUFDO2dCQUhHLHdDQUFXLEdBQVgsVUFBWSxVQUFpQjtvQkFDekIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ3hDLENBQUM7Z0JBWEw7b0JBQUMsZ0JBQVMsQ0FBQzt3QkFDUCxRQUFRLEVBQUUsY0FBYzt3QkFDeEIsV0FBVyxFQUFFLHNDQUFzQzt3QkFDbkQsT0FBTyxFQUFDLENBQUMsZUFBZSxDQUFDO3FCQUM1QixDQUFDOztzQ0FBQTtnQkFRRix5QkFBQztZQUFELENBUEEsQUFPQyxJQUFBO1lBUEQsbURBT0MsQ0FBQSIsImZpbGUiOiJzZWFyY2gvY29tcG9uZW50L3NlYXJjaC1ib3guY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgT3V0cHV0fSBmcm9tICdhbmd1bGFyMi9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdbc2VhcmNoLWJveF0nLFxuICAgIHRlbXBsYXRlVXJsOiAndGVtcGxhdGVzL3NlYXJjaC9zZWFyY2gtYm94LnRwbC5odG1sJyxcbiAgICBvdXRwdXRzOlsnc2VhcmNoQ2hhbmdlZCddXG59KVxuZXhwb3J0IGNsYXNzIFNlYXJjaEJveENvbXBvbmVudCB7XG5cbiAgICBzZWFyY2hDaGFuZ2VkID0gbmV3IEV2ZW50RW1pdHRlcjxzdHJpbmc+KCk7XG4gICAgXG4gICAgc2VhcmNoRXZlbnQoc2VhcmNoVGV4dDpzdHJpbmcpe1xuICAgICAgICB0aGlzLnNlYXJjaENoYW5nZWQuZW1pdChzZWFyY2hUZXh0KTtcbiAgICB9XG59XG4iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=

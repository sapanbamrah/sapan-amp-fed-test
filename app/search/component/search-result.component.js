System.register(['angular2/core', './search-item.component', '../service/people-list.service', '../pipe/search-pipe'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, search_item_component_1, people_list_service_1, search_pipe_1;
    var SearchResultComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (search_item_component_1_1) {
                search_item_component_1 = search_item_component_1_1;
            },
            function (people_list_service_1_1) {
                people_list_service_1 = people_list_service_1_1;
            },
            function (search_pipe_1_1) {
                search_pipe_1 = search_pipe_1_1;
            }],
        execute: function() {
            SearchResultComponent = (function () {
                function SearchResultComponent(_peopleListService) {
                    this._peopleListService = _peopleListService;
                    this.searchText = '';
                    this.response = Array();
                }
                SearchResultComponent.prototype.ngOnInit = function () {
                    this.searchItems = this._peopleListService.fetchList();
                    this.itemsCount = this.searchItems.length;
                };
                SearchResultComponent = __decorate([
                    core_1.Component({
                        selector: '[search-result]',
                        templateUrl: 'templates/search/search-result.tpl.html',
                        directives: [search_item_component_1.SearchItemComponent],
                        providers: [people_list_service_1.PeopleListService],
                        pipes: [search_pipe_1.SearchPipe]
                    }), 
                    __metadata('design:paramtypes', [people_list_service_1.PeopleListService])
                ], SearchResultComponent);
                return SearchResultComponent;
            }());
            exports_1("SearchResultComponent", SearchResultComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlYXJjaC9jb21wb25lbnQvc2VhcmNoLXJlc3VsdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFZQTtnQkFPSSwrQkFBb0Isa0JBQXFDO29CQUFyQyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW1CO29CQUh6RCxlQUFVLEdBQUcsRUFBRSxDQUFDO29CQUNoQixhQUFRLEdBQUcsS0FBSyxFQUFZLENBQUM7Z0JBRThCLENBQUM7Z0JBRTVELHdDQUFRLEdBQVI7b0JBQ0ksSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxFQUFFLENBQUM7b0JBQ3ZELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7Z0JBQzlDLENBQUM7Z0JBbkJMO29CQUFDLGdCQUFTLENBQUM7d0JBQ1AsUUFBUSxFQUFFLGlCQUFpQjt3QkFDM0IsV0FBVyxFQUFFLHlDQUF5Qzt3QkFDdEQsVUFBVSxFQUFDLENBQUMsMkNBQW1CLENBQUM7d0JBQ2hDLFNBQVMsRUFBQyxDQUFDLHVDQUFpQixDQUFDO3dCQUM3QixLQUFLLEVBQUMsQ0FBQyx3QkFBVSxDQUFDO3FCQUNyQixDQUFDOzt5Q0FBQTtnQkFlRiw0QkFBQztZQUFELENBZEEsQUFjQyxJQUFBO1lBZEQseURBY0MsQ0FBQSIsImZpbGUiOiJzZWFyY2gvY29tcG9uZW50L3NlYXJjaC1yZXN1bHQuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnR9IGZyb20gJ2FuZ3VsYXIyL2NvcmUnO1xuaW1wb3J0IHtTZWFyY2hJdGVtQ29tcG9uZW50fSBmcm9tICcuL3NlYXJjaC1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQge1Blb3BsZUxpc3RTZXJ2aWNlfSBmcm9tICcuLi9zZXJ2aWNlL3Blb3BsZS1saXN0LnNlcnZpY2UnO1xuaW1wb3J0IHtTZWFyY2hQaXBlfSBmcm9tICcuLi9waXBlL3NlYXJjaC1waXBlJztcbiBcbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnW3NlYXJjaC1yZXN1bHRdJyxcbiAgICB0ZW1wbGF0ZVVybDogJ3RlbXBsYXRlcy9zZWFyY2gvc2VhcmNoLXJlc3VsdC50cGwuaHRtbCcsXG4gICAgZGlyZWN0aXZlczpbU2VhcmNoSXRlbUNvbXBvbmVudF0sXG4gICAgcHJvdmlkZXJzOltQZW9wbGVMaXN0U2VydmljZV0sXG4gICAgcGlwZXM6W1NlYXJjaFBpcGVdXG59KVxuZXhwb3J0IGNsYXNzIFNlYXJjaFJlc3VsdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdHtcbiAgICBpdGVtc0NvdW50Om51bWJlcjtcbiAgICBzZWFyY2hJdGVtczogQXJyYXk8U2VhcmNoSXRlbT47XG4gICAgcmVzcG9uc2U6c3RyaW5nO1xuICAgIHNlYXJjaFRleHQgPSAnJztcbiAgICByZXNwb25zZSA9IEFycmF5PFNlYXJjaEl0ZW0+O1xuICBcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9wZW9wbGVMaXN0U2VydmljZTogUGVvcGxlTGlzdFNlcnZpY2Upe31cbiAgICBcbiAgICBuZ09uSW5pdCgpOmFueXtcbiAgICAgICAgdGhpcy5zZWFyY2hJdGVtcyA9IHRoaXMuX3Blb3BsZUxpc3RTZXJ2aWNlLmZldGNoTGlzdCgpO1xuICAgICAgICB0aGlzLml0ZW1zQ291bnQgPSB0aGlzLnNlYXJjaEl0ZW1zLmxlbmd0aDtcbiAgICB9XG4gIFxufVxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9

System.register(['angular2/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var SearchItemDirective;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            SearchItemDirective = (function () {
                function SearchItemDirective() {
                }
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', Object)
                ], SearchItemDirective.prototype, "searchItem", void 0);
                SearchItemDirective = __decorate([
                    core_1.Directive({
                        selector: 'search-item',
                        templateUrl: 'templates/search/search-item.tpl.html'
                    }), 
                    __metadata('design:paramtypes', [])
                ], SearchItemDirective);
                return SearchItemDirective;
            }());
            exports_1("SearchItemDirective", SearchItemDirective);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlYXJjaC9kaXJlY3RpdmUvc2VhcmNoLWl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lBTUE7Z0JBQUE7Z0JBRUEsQ0FBQztnQkFERztvQkFBQyxZQUFLLEVBQUU7O3VFQUFBO2dCQUxaO29CQUFDLGdCQUFTLENBQUM7d0JBQ1AsUUFBUSxFQUFFLGFBQWE7d0JBQ3ZCLFdBQVcsRUFBRSx1Q0FBdUM7cUJBQ3ZELENBQUM7O3VDQUFBO2dCQUdGLDBCQUFDO1lBQUQsQ0FGQSxBQUVDLElBQUE7WUFGRCxxREFFQyxDQUFBIiwiZmlsZSI6InNlYXJjaC9kaXJlY3RpdmUvc2VhcmNoLWl0ZW0uY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtEaXJlY3RpdmUsIElucHV0fSBmcm9tICdhbmd1bGFyMi9jb3JlJztcblxuQERpcmVjdGl2ZSh7XG4gICAgc2VsZWN0b3I6ICdzZWFyY2gtaXRlbScsXG4gICAgdGVtcGxhdGVVcmw6ICd0ZW1wbGF0ZXMvc2VhcmNoL3NlYXJjaC1pdGVtLnRwbC5odG1sJ1xufSlcbmV4cG9ydCBjbGFzcyBTZWFyY2hJdGVtRGlyZWN0aXZlIHtcbiAgICBASW5wdXQoKSBzZWFyY2hJdGVtO1xufVxuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9

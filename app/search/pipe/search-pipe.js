System.register(['angular2/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var SearchPipe;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            SearchPipe = (function () {
                function SearchPipe() {
                }
                SearchPipe.prototype.transform = function (value, args) {
                    if (value.length === 0) {
                        return value;
                    }
                    var resultArray = [];
                    for (var _i = 0, value_1 = value; _i < value_1.length; _i++) {
                        var item = value_1[_i];
                        if (item.firstName.match('^.*' + args[0] + '.*$')) {
                            resultArray.push(item);
                        }
                    }
                    return resultArray;
                };
                SearchPipe = __decorate([
                    core_1.Pipe({
                        name: 'searchPipe'
                    }), 
                    __metadata('design:paramtypes', [])
                ], SearchPipe);
                return SearchPipe;
            }());
            exports_1("SearchPipe", SearchPipe);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlYXJjaC9waXBlL3NlYXJjaC1waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lBT0E7Z0JBQUE7Z0JBZ0JBLENBQUM7Z0JBZkcsOEJBQVMsR0FBVCxVQUFVLEtBQWtCLEVBQUUsSUFBYTtvQkFDdkMsRUFBRSxDQUFBLENBQUMsS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQSxDQUFDO3dCQUNuQixNQUFNLENBQUMsS0FBSyxDQUFDO29CQUNqQixDQUFDO29CQUVELElBQUksV0FBVyxHQUFHLEVBQUUsQ0FBQztvQkFFckIsR0FBRyxDQUFBLENBQWEsVUFBSyxFQUFMLGVBQUssRUFBTCxtQkFBSyxFQUFMLElBQUssQ0FBQzt3QkFBbEIsSUFBSSxJQUFJLGNBQUE7d0JBQ1IsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFBLENBQUM7NEJBQzdDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQzNCLENBQUM7cUJBQ0o7b0JBRUQsTUFBTSxDQUFDLFdBQVcsQ0FBQztnQkFDdkIsQ0FBQztnQkFsQkw7b0JBQUMsV0FBSSxDQUFDO3dCQUNGLElBQUksRUFBQyxZQUFZO3FCQUNwQixDQUFDOzs4QkFBQTtnQkFpQkYsaUJBQUM7WUFBRCxDQWhCQSxBQWdCQyxJQUFBO1lBaEJELG1DQWdCQyxDQUFBIiwiZmlsZSI6InNlYXJjaC9waXBlL3NlYXJjaC1waXBlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtQaXBlfSBmcm9tICdhbmd1bGFyMi9jb3JlJztcbmltcG9ydCB7UGlwZVRyYW5zZm9ybX0gZnJvbSAnYW5ndWxhcjIvY29yZSc7XG5pbXBvcnQge1NlYXJjaEl0ZW19IGZyb20gJy4uL2ludGVyZmFjZS9zZWFyY2gtaXRlbSc7XG5cbkBQaXBlKHtcbiAgICBuYW1lOidzZWFyY2hQaXBlJ1xufSlcbmV4cG9ydCBjbGFzcyBTZWFyY2hQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybXtcbiAgICB0cmFuc2Zvcm0odmFsdWU6U2VhcmNoSXRlbVtdLCBhcmdzOnN0cmluZ1tdKTphbnl7XG4gICAgICAgIGlmKHZhbHVlLmxlbmd0aCA9PT0gMCl7XG4gICAgICAgICAgICByZXR1cm4gdmFsdWU7XG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGxldCByZXN1bHRBcnJheSA9IFtdO1xuICAgICAgICBcbiAgICAgICAgZm9yKGxldCBpdGVtIG9mIHZhbHVlKXtcbiAgICAgICAgICAgIGlmKGl0ZW0uZmlyc3ROYW1lLm1hdGNoKCdeLionKyBhcmdzWzBdICsgJy4qJCcpKXtcbiAgICAgICAgICAgICAgICByZXN1bHRBcnJheS5wdXNoKGl0ZW0pOyBcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgcmV0dXJuIHJlc3VsdEFycmF5O1xuICAgIH1cbn1cbiJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==

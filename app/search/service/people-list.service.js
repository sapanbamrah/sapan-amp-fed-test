System.register(['angular2/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var PeopleListService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            PeopleListService = (function () {
                function PeopleListService() {
                }
                PeopleListService.prototype.fetchList = function () {
                    var data = [
                        {
                            'id': '1',
                            'firstName': 'Sean',
                            'lastName': 'Kerr',
                            'picture': 'img/sean.png',
                            'Title': 'Senior Developer'
                        },
                        {
                            'id': '2',
                            'firstName': 'Yaw',
                            'lastName': 'Ly',
                            'picture': 'img/yaw.png',
                            'Title': 'AEM Magician'
                        },
                        {
                            'id': '3',
                            'firstName': 'Lucy',
                            'lastName': 'Hehir',
                            'picture': 'img/lucy.png',
                            'Title': 'Scrum master'
                        },
                        {
                            'id': '4',
                            'firstName': 'Rory',
                            'lastName': 'Elrick',
                            'picture': 'img/rory.png',
                            'Title': 'AEM Guru'
                        },
                        {
                            'id': '5',
                            'firstName': 'Eric',
                            'lastName': 'Kwok',
                            'picture': 'img/eric.png',
                            'Title': 'Technical Lead'
                        },
                        {
                            'id': '6',
                            'firstName': 'Hayley',
                            'lastName': 'Crimmins',
                            'picture': 'img/hayley.png',
                            'Title': 'Dev Manager'
                        }
                    ];
                    return data;
                };
                PeopleListService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [])
                ], PeopleListService);
                return PeopleListService;
            }());
            exports_1("PeopleListService", PeopleListService);
        }
    }
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlYXJjaC9zZXJ2aWNlL3Blb3BsZS1saXN0LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFHQTtnQkFBQTtnQkFrREEsQ0FBQztnQkFqREcscUNBQVMsR0FBVDtvQkFFQSxJQUFJLElBQUksR0FBRzt3QkFDWDs0QkFDSSxJQUFJLEVBQUUsR0FBRzs0QkFDVCxXQUFXLEVBQUUsTUFBTTs0QkFDbkIsVUFBVSxFQUFFLE1BQU07NEJBQ2xCLFNBQVMsRUFBRSxjQUFjOzRCQUN6QixPQUFPLEVBQUUsa0JBQWtCO3lCQUM5Qjt3QkFDRDs0QkFDSSxJQUFJLEVBQUUsR0FBRzs0QkFDVCxXQUFXLEVBQUUsS0FBSzs0QkFDbEIsVUFBVSxFQUFFLElBQUk7NEJBQ2hCLFNBQVMsRUFBRSxhQUFhOzRCQUN4QixPQUFPLEVBQUUsY0FBYzt5QkFDMUI7d0JBQ0Q7NEJBQ0ksSUFBSSxFQUFFLEdBQUc7NEJBQ1QsV0FBVyxFQUFFLE1BQU07NEJBQ25CLFVBQVUsRUFBRSxPQUFPOzRCQUNuQixTQUFTLEVBQUUsY0FBYzs0QkFDekIsT0FBTyxFQUFFLGNBQWM7eUJBQzFCO3dCQUNEOzRCQUNJLElBQUksRUFBRSxHQUFHOzRCQUNULFdBQVcsRUFBRSxNQUFNOzRCQUNuQixVQUFVLEVBQUUsUUFBUTs0QkFDcEIsU0FBUyxFQUFFLGNBQWM7NEJBQ3pCLE9BQU8sRUFBRSxVQUFVO3lCQUN0Qjt3QkFDRDs0QkFDSSxJQUFJLEVBQUUsR0FBRzs0QkFDVCxXQUFXLEVBQUUsTUFBTTs0QkFDbkIsVUFBVSxFQUFFLE1BQU07NEJBQ2xCLFNBQVMsRUFBRSxjQUFjOzRCQUN6QixPQUFPLEVBQUUsZ0JBQWdCO3lCQUM1Qjt3QkFDRDs0QkFDSSxJQUFJLEVBQUUsR0FBRzs0QkFDVCxXQUFXLEVBQUUsUUFBUTs0QkFDckIsVUFBVSxFQUFFLFVBQVU7NEJBQ3RCLFNBQVMsRUFBRSxnQkFBZ0I7NEJBQzNCLE9BQU8sRUFBRSxhQUFhO3lCQUN6QjtxQkFDSixDQUFDO29CQUVNLE1BQU0sQ0FBQyxJQUFJLENBQUM7Z0JBQ2hCLENBQUM7Z0JBbERMO29CQUFDLGlCQUFVLEVBQUU7O3FDQUFBO2dCQW1EYix3QkFBQztZQUFELENBbERBLEFBa0RDLElBQUE7WUFsREQsaURBa0RDLENBQUEiLCJmaWxlIjoic2VhcmNoL3NlcnZpY2UvcGVvcGxlLWxpc3Quc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnYW5ndWxhcjIvY29yZSc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBQZW9wbGVMaXN0U2VydmljZXtcbiAgICBmZXRjaExpc3QoKXtcblxuICAgIGxldCBkYXRhID0gW1xuICAgIHtcbiAgICAgICAgJ2lkJzogJzEnLFxuICAgICAgICAnZmlyc3ROYW1lJzogJ1NlYW4nLFxuICAgICAgICAnbGFzdE5hbWUnOiAnS2VycicsXG4gICAgICAgICdwaWN0dXJlJzogJ2ltZy9zZWFuLnBuZycsXG4gICAgICAgICdUaXRsZSc6ICdTZW5pb3IgRGV2ZWxvcGVyJ1xuICAgIH0sXG4gICAgeyBcbiAgICAgICAgJ2lkJzogJzInLFxuICAgICAgICAnZmlyc3ROYW1lJzogJ1lhdycsXG4gICAgICAgICdsYXN0TmFtZSc6ICdMeScsXG4gICAgICAgICdwaWN0dXJlJzogJ2ltZy95YXcucG5nJyxcbiAgICAgICAgJ1RpdGxlJzogJ0FFTSBNYWdpY2lhbidcbiAgICB9LFxuICAgIHtcbiAgICAgICAgJ2lkJzogJzMnLFxuICAgICAgICAnZmlyc3ROYW1lJzogJ0x1Y3knLFxuICAgICAgICAnbGFzdE5hbWUnOiAnSGVoaXInLFxuICAgICAgICAncGljdHVyZSc6ICdpbWcvbHVjeS5wbmcnLFxuICAgICAgICAnVGl0bGUnOiAnU2NydW0gbWFzdGVyJ1xuICAgIH0sXG4gICAge1xuICAgICAgICAnaWQnOiAnNCcsXG4gICAgICAgICdmaXJzdE5hbWUnOiAnUm9yeScsXG4gICAgICAgICdsYXN0TmFtZSc6ICdFbHJpY2snLFxuICAgICAgICAncGljdHVyZSc6ICdpbWcvcm9yeS5wbmcnLFxuICAgICAgICAnVGl0bGUnOiAnQUVNIEd1cnUnXG4gICAgfSxcbiAgICB7XG4gICAgICAgICdpZCc6ICc1JyxcbiAgICAgICAgJ2ZpcnN0TmFtZSc6ICdFcmljJyxcbiAgICAgICAgJ2xhc3ROYW1lJzogJ0t3b2snLFxuICAgICAgICAncGljdHVyZSc6ICdpbWcvZXJpYy5wbmcnLFxuICAgICAgICAnVGl0bGUnOiAnVGVjaG5pY2FsIExlYWQnXG4gICAgfSxcbiAgICB7XG4gICAgICAgICdpZCc6ICc2JyxcbiAgICAgICAgJ2ZpcnN0TmFtZSc6ICdIYXlsZXknLFxuICAgICAgICAnbGFzdE5hbWUnOiAnQ3JpbW1pbnMnLFxuICAgICAgICAncGljdHVyZSc6ICdpbWcvaGF5bGV5LnBuZycsXG4gICAgICAgICdUaXRsZSc6ICdEZXYgTWFuYWdlcidcbiAgICB9XG5dO1xuXG4gICAgICAgIHJldHVybiBkYXRhO1xuICAgIH1cbn0iXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
